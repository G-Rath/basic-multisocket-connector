package nz.zhang;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Server
 * Created by G-Rath on 23/05/2015.
 */
public class Server
{
	public final static int SERVER_PORT = 1225; //As picked from "Baby Kaisius"

	private final static Server INSTANCE = new Server();

	private ServerSocket serverSocket;

	private ArrayList<Socket> sockets = new ArrayList<Socket>();

	public static Server getInstance()
	{
		return INSTANCE;
	}

	public void start()
	{
		try
		{
			serverSocket = new ServerSocket( SERVER_PORT );
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}

		createConnectionHandle();
	}

	public void passSocket( final Socket newSocket )
	{
		System.out.println( "Getting new connection" );

		sockets.add( newSocket );

		//Goes in another class. For each socket that is passed a new instance of "SocketHandler" (whatever you want to call it) is made.
		//The new client socket gets passed to the new SocketHandler, which represents the client on the server's end.
		//Its the job of the SocketHandler to check the socket for new data, and send data from the server.
		new Thread( new Runnable()
		{
			private Socket socket = newSocket;

			private BufferedReader socketIn;

			public void run()
			{
				try
				{
					socketIn = new BufferedReader( new InputStreamReader( socket.getInputStream(), "UTF-8" ) );
					String line;

					//noinspection InfiniteLoopStatement
					while( true )
					{
						while( ( line = socketIn.readLine() ) != null )
						{
							System.out.println( "person says: " + line );
						}
					}
				}
				catch( IOException e )
				{
					e.printStackTrace();
				}
			}
		} ).start();
	}

	public void createConnectionHandle()
	{
		//This would be done in a NEW CLASS. Class calls server and passes connection.
		//This MUST be done on a separate thread since .accept() is BLOCKING. (It will pause the thread its on until it returns).
		new Thread( new Runnable()
		{
			public void run()
			{
				//noinspection InfiniteLoopStatement
				while( true )
				{
					try
					{
						Socket clientSocket = serverSocket.accept();
						Server.getInstance().passSocket( clientSocket );
					}
					catch( IOException e )
					{
						e.printStackTrace();
					}
				}
			}
		} ).start();
	}
}
