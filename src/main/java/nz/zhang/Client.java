package nz.zhang;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Client
 * Created by G-Rath on 23/05/2015.
 */
public class Client
{
	private final static Client INSTANCE = new Client();

	private PrintWriter socketWriter;
	private BufferedReader socketReader;

	private Socket conSocket;

	private String hostName = "localhost";

	private Scanner console;

	private boolean scan = true;

	public static Client getInstance()
	{
		return INSTANCE;
	}

	private void setupScanner()
	{
		console = new Scanner( System.in );
	}

	public void start()
	{
		try
		{
			conSocket = new Socket( hostName, Server.SERVER_PORT );

			socketWriter = new PrintWriter( new BufferedWriter( new OutputStreamWriter( conSocket.getOutputStream(), "UTF-8" ) ), true );
			socketReader = new BufferedReader( new InputStreamReader( conSocket.getInputStream(), "UTF-8" ) );

			setupScanner();

			new Thread( new Runnable()
			{
				@Override
				public void run()
				{
					while( scan )
					{
						if( console.hasNextLine() )
						{
							String command = console.nextLine();
							System.out.println( ">> " + command );
							Client.getInstance().sendServerMessage( command );
						}
					}
				}
			} ).start();

			//noinspection InfiniteLoopStatement
			while( true )
			{
				String line = "";
				while( ( line = readMessageFromServer() ) != null )
				{
					System.out.println( "server says: " + line );
				}
			}
		}
		catch( IOException e )
		{
			//e.printStackTrace();
			System.out.println( "failed to connect to server " );
		}
	}

	public void sendServerMessage( String message )
	{
		socketWriter.println( message );
	}

	public String readMessageFromServer() throws IOException
	{
		return socketReader.readLine();
	}

	public String getHostName()
	{
		return hostName;
	}

	public void setHostName( String hostName )
	{
		this.hostName = hostName;
	}
}
