package nz.zhang;

import java.util.Collections;
import java.util.HashSet;

/**
 * Starter
 * Created by G-Rath on 23/05/2015.
 */
public class Starter
{
	/**
	 * To start server create a configuration with "server" as a parameter.
	 * To start a client do nothing extra.
	 * @param args
	 */
	public static void main( String[] args )
	{
		HashSet<String> arguments = new HashSet<>();
		Collections.addAll( arguments, args );

//		for( String s : args )
//		{
//			arguments.add( s );
//			//System.out.println( s );
//		}

		if( arguments.contains( "headless" ) )
		{
			System.setProperty( "java.awt.headless", "true" );
			System.out.println( "Running in HEADLESS mode" );
		}

		String customHost = "";

		for( String s : args )
		{
			if( s.startsWith( "host:" ) )
			{
				customHost = s.substring( s.indexOf( ":" ) + 1 );
				System.out.println( "Custom host of " + customHost );
				Client.getInstance().setHostName( customHost );
			}
		}

		//System.out.println( )

		if( arguments.contains( "server" ) )
			Server.getInstance().start();
		else
			Client.getInstance().start();
	}
}
